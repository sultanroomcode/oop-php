<?php
class Animal
{
    public $name;
    public $legs;
    public $cold_blooded;

    public function __construct($name)
    {
        $this->setName($name);
        $this->setLegs(2);
        $this->setBlood(false);
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setLegs($legs)
    {
        $this->legs = (int) $legs;
    }

    public function setBlood($cold)
    {
        $this->cold_blooded = $cold;
    }

    public function getBlood()
    {
        return $this->cold_blooded ? 'true' : 'false';
    }
}

<?php
class Frog extends Animal
{
    public function __construct($name)
    {
        $this->setName($name);
        $this->setLegs(4);
        $this->setBlood(false);
    }

    public function jump()
    {
        echo 'hop hop';
    }
}

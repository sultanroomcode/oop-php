INSERT INTO `customers`(`id`, `name`, `email`, `password`) VALUES
(null, 'John Doe', 'john@doe.com', 'john123'),
(null, 'Jane Doe', 'jane@doe.com', 'jenita123');

INSERT INTO `orders`(`id`, `amount`, `customer_id`) VALUES
(null, '500', 1),
(null, '200', 2),
(null, '750', 2),
(null, '250', 1),
(null, '400', 2);
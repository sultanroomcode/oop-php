SELECT
c.name customer_name,
SUM(o.amount) total_amount
FROM `orders` o
LEFT JOIN customers c ON c.id = o.customer_id
GROUP BY o.customer_id
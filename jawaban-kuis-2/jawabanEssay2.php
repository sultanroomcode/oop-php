<?php
function perolehanMedali($arr)
{
    $abb = [];
    if (count($arr) > 0) {

        $abb = [];
        $country = [];
        $i = 0;
        foreach ($arr as $v) {
            $abb[$v[0]][$v[1]] = 0;
            $country[] = $v[0];
        }

        foreach ($arr as $v) {
            $abb[$v[0]][$v[1]] += 1;
        }

        //uniq
        $newcountry = array_unique($country);
        $r = [];
        foreach ($newcountry as $v) {
            $r[] = [
                'negara' => $v,
                'emas' => (isset($abb[$v]['emas'])) ? $abb[$v]['emas'] : 0,
                'perak' => (isset($abb[$v]['perak'])) ? $abb[$v]['perak'] : 0,
                'perunggu' => (isset($abb[$v]['perunggu'])) ? $abb[$v]['perunggu'] : 0
            ];
        }
    } else {
        return 'no-data';
    }

    return $r;
}

$arr = [
    ['Indonesia', 'emas'],
    ['India', 'perak'],
    ['Korea Selatan', 'perak'],
    ['India', 'perak'],
    ['India', 'emas'],
    ['Indonesia', 'perak'],
    ['Indonesia', 'emas'],
    ['Palestina', 'emas'],
];

var_dump(perolehanMedali($arr));

<?php
require 'class/Animal.php';
require 'class/Ape.php';
require 'class/Frog.php';

$sheep = new Animal("Shaun");
echo $sheep->name; // "shaun"
echo $sheep->legs; // 2
echo $sheep->getBlood(); // false

echo '<hr>';
$sungokong = new Ape("kera sakti");
echo $sungokong->name; // kera sakti
echo $sungokong->legs; // 2
$sungokong->yell(); // "Auooo"

echo '<hr>';
$kodok = new Frog("buduk");
echo $kodok->name; // buduk
echo $kodok->legs; // 4
$kodok->jump(); // "hop hop"